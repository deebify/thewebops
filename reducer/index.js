const intiState = {
    fetching: false,
    fetched: false,
    images: [],
    err: null
}

const reducer = (state = intiState, action) => {
    switch (action.type) {
        case 'req_PENDING':
            return { ...state, fetching: true }
        case 'req_FULFILLED':
            return { ...state, images: [...state.images,...action.payload.data], fetched: true, fetching: false }
        case 'req_REJECTED':
            return { ...state, fetched: false, fetching: false, err: action.payload }
        default:
            return state
    }
}

export default reducer;
