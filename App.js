import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {Header} from 'react-native-elements'
import ImageList from './components/ImageList'

import {createStore, applyMiddleware} from 'redux'
import {Provider} from 'react-redux'
import { createLogger } from 'redux-logger'
import axios from 'axios'
import promise from 'redux-promise-middleware'

// My Reducer Logic
import reducer from './reducer'

// Redux Middleware
const middleware = applyMiddleware(promise(), createLogger())
// Redux Store
const store = createStore(reducer,middleware)

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
      <View>
      <Header
        leftComponent={{ icon: 'menu', color: '#fff' }}
        centerComponent={{ text: 'TheWebOps', style: { color: '#fff' } }}
        rightComponent={{ icon: 'home', color: '#fff' }}
        />

        <ImageList />
      </View>
      </Provider>
    );
  }
}
