import React, { Component } from 'react'
import { View, Text, FlatList, Image, ActivityIndicator,Dimensions } from 'react-native'
import { List, ListItem } from 'react-native-elements'
import axios from 'axios'
import { connect } from 'react-redux'
var {height, width} = Dimensions.get('window');


class ImageList extends Component {

    componentDidMount() {
        this.props.request();
    }

    footer = ()=>{
        if(!this.props.fetching) return null
        return (
            <View style={{paddingVertical:20}}>
                <ActivityIndicator animating size="large" />
            </View>
            
            );
        
    }
    
    handleRefresh = ()=>{
        this.props.request()
    }
    
    handleSeeMore = ()=>{
        this.props.request()
    }
    
    seperator = ()=>{
        return <View style={{ 
            height : 10,
        }}/>
    }
    

    render() {
        const { fetching, fetched, err, images } = this.props
            return (
                <List>
                    <FlatList
                        style={{padding:10}}
                        data = {images}
                        renderItem = {(image)=>{
                        const uri = image.item.image
                        return (
                        <Image
                            source={{ uri: uri }}
                            style={{width: width, height: 300 }}
                            />
                        )}}
                        keyExtractor = {(image)=>image.index}
                        ListFooterComponent={this.footer}
                        refreshing={this.props.fetching}
                        onRefresh={this.handleRefresh}
                        onEndReached={this.handleSeeMore}
                        onEndThreshold={3}
                        ItemSeparatorComponent={this.seperator}
                    />
                </List>
            );
    }

}
const mapStateToProps = state => state

const mapDispatchToProps = (dispatch) => {
    return {
        request: () => dispatch({
            type: 'req',
            payload: axios("https://protected-dawn-18972.herokuapp.com/data")
        })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ImageList)
